<a href="https://www.devops.cprime.com/" target="_blank">
<img src=".assets/logo.png" width="300" />
</a>

[![Maintained by CPrime Engineering](https://img.shields.io/badge/maintained%20by-cprime%20engineering-ED1846?style=flat-square)](https://www.devops.cprime.com/) 
[![Built for Engineers](https://img.shields.io/badge/project-devops%20library-ED1846?style=flat-square)](https://www.devops.cprime.com/library)
[![Latest](https://img.shields.io/badge/latest-0.0.0-green?style=flat-square)](../../releases) 

# devops-library-cicd-harness
This repository contains CICD configuration code to support the CPrime DevOps Library.

## Quick start

[![Quick Start Video](/.assets/video-graphics.png)](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

## Who maintains this repo?

This repo is maintained by CPrime Engineering. If you're looking for support, send an email to [engineering@cprime.com](mailto:engineering@cprime.com?subject=DevOps%20Library%20VPC%20AWS).
CPrime Engineering can help with:

- Setup, customization, and support for this repo.
- Modules for other types of infrastructure, such as VPCs, Docker clusters, databases, and continuous integration.
- Modules that meet compliance requirements, such as HIPAA.
- Consulting & Training on AWS, Terraform, and DevOps.

## How do I contribute to this repo?

Contributions are welcome. Check out the
[Contribution Guidelines](/CONTRIBUTING.md) and 
[Code of Conduct](/CONDUCT.md) for instructions.

## How is this repo versioned?

This repo follows the principles of [Semantic Versioning](http://semver.org/). You can find each new release,
along with the changelog, in the [Releases Page](../../releases).

During initial development, the major version will be 0 (e.g., `0.x.y`), which indicates the code does not yet have a
stable API. Once we hit `1.0.0`, we will make every effort to maintain a backwards compatible API and use the MAJOR,
MINOR, and PATCH versions on each release to indicate any incompatibilities.

Publication of the CHANGELOG for this repo is automated using [git-changelog](https://github.com/git-chglog/git-chglog) in the style of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Architectural Decisions

All architectural decisions made in the authoring of this repo are captured as a log of [Architecture Decision Records](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions) (ADRs). This log can be found in the [docs/en/adr](docs/en/adr) sub directory of this repo.

Creation of architecural decision records relating to this repo is automated using [adr-tools](https://github.com/npryce/adr-tools).

## License

This code is released under the Apache 2.0 License. Please see
[LICENSE](/LICENSE) and
[NOTICE](/NOTICE) for more details.

<a href="https://opensource.org/" target="_blank">
<img src=".assets/cp-osi-love.png" />
</a>
